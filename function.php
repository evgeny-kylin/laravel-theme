<?php
define('THEME_URL', get_template_directory_uri().'/laravel/public/');

register_nav_menus( [
    'header' => __('Header menu', 'theme-laravel'),
    'footer' => __('Footer menu', 'theme-laravel'),
] );

add_action( 'init', 'functions_boot' );
function functions_boot()
{
    add_image_size( 'large_c', '1680', '640', true );
    add_image_size( 'hdm_c', '1200', '480', true );
    add_image_size( 'product_l', '960', '960' );
}

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page('Settings');
}
remove_action('template_redirect', 'redirect_canonical');
add_theme_support('post-thumbnails');


require_once __DIR__.'/post_types.php';
require_once __DIR__.'/acf.php';

function asset_theme($path = ''){
    return THEME_URL.$path;
}