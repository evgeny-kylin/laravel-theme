<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');


$options = preg_replace('/(%)([a-zA-Z]*)(%)/', '{$2}', \Corcel\Model\Option::get('permalink_structure'));

Route::get($options, 'PostController@index');
