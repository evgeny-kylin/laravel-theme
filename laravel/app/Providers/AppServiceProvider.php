<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use App\Helpers\TrelloClient;
use Corcel\Model\Option;
use App\Helpers\Menu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Helpers\Front::private_policy_page();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::singleton('Option', Option::class);
        App::singleton('Menu', Menu::class);
        App::singleton('Trello', TrelloClient::class);
    }
}
