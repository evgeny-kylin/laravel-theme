<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use Automattic\WooCommerce\Client as WooClient;

class WooProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::singleton('WooCommerce', function()
        {
            return new WooClient(
                defined('WP_SITEURL') ? WP_SITEURL : env('WOO_URL', ''),
                env('WOO_KEY'),
                env('WOO_SECRET'),
                [
                    'wp_api' => true,
                    'version' => 'wc/v2',
                    'CURLOPT_FAILONERROR' => false
                ]
            );
        });
    }
}
