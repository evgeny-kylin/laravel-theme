<?php

namespace App\Models;

use App\Traits\LangTrait;
use Corcel\Model\Meta\Meta;
use Corcel\Model\Post as PostCorcel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use App\Traits\TermTrait;
use Illuminate\Support\Str;

class Post extends PostCorcel
{
    use TermTrait;

    protected $postType = 'post';

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('publish', function (Builder $builder) {
            $builder->where('post_status', 'publish');
        });
    }

    public function getThumbAttribute()
    {
        if($thumbnail = $this->thumbnail){
            return self::thumbSizes($thumbnail);
        }

        return null;
    }

    public static function thumbSizes($thumbnail)
    {


        if(!isset($thumbnail->attachment->meta->_wp_attachment_metadata)){
            return null;
        }
        if($thumbnail->attachment){
            return self::thumbAttachmentSizes($thumbnail->attachment);
        }
        return null;
    }

    protected static function thumbAttachmentSizes($attachment)
    {
        global $_wp_additional_image_sizes;
        $meta = unserialize($attachment->meta->_wp_attachment_metadata);
        $sizes = array_get($meta, 'sizes');

        foreach($sizes as $key => $size){
            $sizes[$key]['url'] = dirname($attachment->url).'/'. $size['file'];
            if(isset($_wp_additional_image_sizes[$key])){
                unset($_wp_additional_image_sizes[$key]);
            }
        }
        if(count($_wp_additional_image_sizes)){
            foreach($_wp_additional_image_sizes as $key => $value){
                $sizes[$key] = $value;
                $sizes[$key]['url'] = $sizes['thumbnail']['url'];
            }
        }
        return $sizes;
    }

    public function getDescriptionAttribute()
    {
        if(strlen($this->post_excerpt) > 100){
            $cut_text = substr($this->post_excerpt, 0, 100);
            $pos = strripos($cut_text, ' ');
            $cut_text = substr($this->post_excerpt, 0, $pos);
            return $cut_text.' ...';
        }
        $this->post_excerpt;
    }

    public function postRoute()
    {
        return $this->postType;
    }

    public function getUrlAttribute()
    {
        return route($this->postRoute(), ['slug' => $this->slug]);
    }

    public function related($limit = 5)
    {
        $categories = Arr::get($this->terms,'category', collect())->pluck('term_id');
        return Post::query()->hasterm($categories->toArray())->limit($limit)->get();
    }

    public static function firstBySlug($slug)
    {
        return static::where('post_name', urlencode($slug))->first();
    }

    public function metam($attr)
    {
        if($count = $this->meta->$attr){
            $metas = $this->meta->filter(function($item) use ($attr){
                return Str::startsWith($item->meta_key, $attr.'_');
            })->mapToGroups(function($item){
                $name = explode('_', $item->meta_key);
                return [$name[1] => $item];
            })->map(function ($item){
                return $item->mapWithKeys(function ($item){
                    $name = explode('_', $item->meta_key);
                    return [$name[2] => $item->meta_value];
                })->toArray();
            });

            return $metas;
        }
        return collect();
    }
    public function metaps($attr, $class)
    {
        if($ids = @unserialize($this->meta->$attr)) {
            return $class::find($ids);
        }
            return collect();
    }

}
