<?php

namespace App\Models;

use App\Traits\LangTrait;
use Illuminate\Database\Eloquent\Model;
use Corcel\Model\Taxonomy as TaxonomyCorcel;

class Taxonomy extends TaxonomyCorcel
{
    use LangTrait;

    public function getTerm($class)
    {
        return $this->belongsTo($class, 'term_id', 'term_id')->first();
    }

    public function getUrlAttribute()
    {
        return route($this->raxonomy, ['slug' => $this->term->slug]);
    }

    public function term($class = null)
    {
        return $this->belongsTo($class ?? Term::class, 'term_id');
    }
}
