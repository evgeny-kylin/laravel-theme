<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Post;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PostController extends Controller
{
    protected $postType = 'post';

    protected $postModel = Post::class;

    protected $post;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->post = $this->postModel::firstBySlug($request->slug);
        View::share('post', $this->post);
    }

    public function index(Request $request)
    {
        $posts = Post::paginate();
        return view($this->postType.'.index', [
            'posts' => $posts
        ]);
    }

    public function show(Request $request)
    {
        if(!$this->post){
            abort(404);
        }

        $this->title = $this->post->title;
        return view($this->postType.'.single');
    }
}
