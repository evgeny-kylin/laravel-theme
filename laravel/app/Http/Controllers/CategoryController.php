<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Term;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function show(Request $request, $category)
    {
        $category = Term::slug($category)->firstOrFail();
        $posts = Post::query()->hasterm($category)->paginate();
        $this->title = $category->name;
        return view('category.single', [
            'category' => $category,
            'posts' => $posts
        ]);
    }
}
