<?php

namespace App\Http\Controllers;

use App\Helpers\Menu;
use App\Models\Taxonomy;
use App\Models\Term;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $title;

    public function __construct()
    {
        View::share([
            'controller' => $this,
            'data' => [

            ]
        ]);
    }

    public function getMenu($location)
    {
        return Menu::get($location);
    }

    public function getTitle()
    {
        return config('app.name').($this->title ? ' | '.$this->title : '');
    }

    public function getDescription()
    {
        return '';
    }

}
