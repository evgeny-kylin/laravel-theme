<?php

namespace App\Http\Controllers;

use App\Models\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class TermController extends Controller
{
    protected $postType = 'category';

    protected $post;

    protected $model = Term::class;

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function index(Request $request)
    {
        return view($this->postType.'index');
    }

    public function show(Request $request, $tag)
    {
        $tag = $this->model::slug($tag)->firstOrFail();
        $posts = $this->childClass::query()->hasterm($tag)->paginate();
        $this->title = $tag->name;
        return view($this->postType.'.single', [
            'term' => $tag,
            'posts' => $posts
        ]);
    }
}
