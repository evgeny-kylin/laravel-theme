<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PageController extends Controller
{
    protected $postType = 'page';

    protected $post;

    public function show(Request $request, $slug1, $slug2 = null, $slug3 = null)
    {
        if(!$slug3 && !$slug2){
            $page = Page::firstBySlug($slug1);
            if(!$page){
                abort(404);
            }
        }elseif(!$slug3){
            $page = Page::firstBySlug($slug2);
            if(!$page || !$page->parent || !$page->parent->slug == $slug1){
                abort(404);
            }
        }else{
            $page = Page::firstBySlug($slug3);
            if(!$page || !$page->parent || !$page->parent->slug == $slug2){
                abort(404);
            }
            if(!$page->parent->parent || !$page->parent->parent->slug == $slug1){
                abort(404);
            }
        }
        $this->title = $page->title;
        return view($this->postType.'.single',[
            'post' => $page
        ]);
    }
}
