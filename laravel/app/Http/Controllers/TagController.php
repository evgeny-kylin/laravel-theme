<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Term;
use Illuminate\Http\Request;

class TagController extends Controller
{

    public function show(Request $request, $tag)
    {
        $tag = Term::slug($tag)->firstOrFail();
        $posts = Post::query()->hasterm($tag)->paginate();
        $this->title = $tag->name;
        return view('tag.single', [
            'tag' => $tag,
            'posts' => $posts
        ]);
    }
}
