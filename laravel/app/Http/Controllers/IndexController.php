<?php

namespace App\Http\Controllers;

use App\Models\BuyOrder;
use App\Models\Page;
use App\Helpers\Front;
use Corcel\Model\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $post = Front::home_page();
        return view('index', ['post' => $post]);
    }

    public function search(Request $request)
    {
        $request->validate([
            's' => 'string'
        ]);

        $search = $request->get('s', '');

        $result = DB::table('posts')
            ->select(['id', 'post_type'])

            ->where('post_content', 'like', '%'.$search.'%')
            ->whereIn('post_type', config('wp.search.post_types'))
            ->where('post_status', 'publish')
            ->orderBy('post_date', 'DESC')
            ->paginate();

        $posts = collect();

        foreach(config('wp.search.post_types') as $post_type){
            $resultPostType = (config('wp.post_types.'.$post_type))::whereIn('ID', $result->map(function($item) use ($post_type){
                if($post_type == $item->post_type){
                    return $item->id;
                }
            })->filter())->get();

            $posts = $posts->merge($resultPostType);
        }

        return view('search', [
            's' => $search,
            'posts' => $posts->sortByDesc('post_date'),
            'paginate' => $result
        ]);
    }

    public function privatePolicy()
    {
        $page = Page::withoutGlobalScopes()->findOrFail(Option::get('wp_page_for_privacy_policy'));
        return view('page.static', [
            'post' => $page
        ]);
    }
}
