<?php

namespace App\Traits;


use App\Models\Taxonomy;

trait TermTrait
{
    public function scopeHasterm($query, $term = null)
    {
        if(is_object($term)){
            $term = $term->term_id;
        }
        $query->whereHas('taxonomies', function($query)use ($term){
            $query->whereHas('term', function ($query) use ($term){
                if(is_array($term)){
                    $query->whereIn('term_id', $term);
                }else{
                    $query->where('term_id', $term);
                }

            });
        });

        return $query;
    }

    public function getTerms($class)
    {
        return $this->getTermsAttribute($class);
    }

    public function getTermsAttribute($class = null)
    {
        return $this->taxonomies->groupBy(function ($taxonomy) {
            return $taxonomy->taxonomy == 'post_tag' ?
                'tag' : $taxonomy->taxonomy;
        })->map(function ($group) use ($class){
            return $group->mapWithKeys(function ($item) use ($class){
                return [$item->term->slug => $item->term($class)->first()];
            });
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function taxonomies()
    {
        return $this->belongsToMany(
            Taxonomy::class, 'term_relationships', 'object_id', 'term_taxonomy_id'
        );
    }
}