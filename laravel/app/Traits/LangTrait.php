<?php

namespace App\Traits;


use App\Models\Taxonomy;
use App\Models\Term;
use Corcel\Model\TermRelationship;

trait LangTrait
{
    public function postRoute()
    {
        return app()->getLocale().'.'.$this->postType;
    }

    public function langRelationshipTerm()
    {
        return $this->belongsToMany(
            Taxonomy::class,
            'term_relationships',
            'object_id',
            'term_taxonomy_id',
            'term_id')->where('taxonomy', 'term_language')->where('term_id', $this->currentLangTerm());
    }

    public function langRelationshipPost()
    {
        return $this->belongsToMany(
            Taxonomy::class,
            'term_relationships',
            'object_id',
            'term_taxonomy_id',
            'ID',
            'term_id')->where('taxonomy', 'post_translations');
    }

    public function getLangPostAttribute()
    {
        if($term = $this->langRelationshipPost->first()){
            $langs = collect(@unserialize($term->description ?? '') ?? []);
            if($id = $langs->get(app()->getLocale())){
                return (self::class)::find($id);
            }else{
                $id = $langs->get(config('languages.languages'));
                return (self::class)::find($id);
            }
        }
        return $this;
    }

    public function currentLangTerm()
    {
        if($term = Term::where('slug', 'pll_'.app()->getLocale())->first()){
            return $term->id;
        };
        return 0;
    }

    public function currentLang()
    {
        $langs = collect(config('languages.languages'))->keyBy('lang');
        if($lang = $langs->get(app()->getLocale())){
            return $lang['code'];
        }else{
            $langs->get(config('languages.default'));
            return $lang['code'];
        }
    }

    public function scopeHasLangTerm($query)
    {
        $query->whereHas('langRelationshipTerm');
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function term()
    {
        return $this->belongsTo(Term::class, 'term_id')->hasLangTerm();
    }
}