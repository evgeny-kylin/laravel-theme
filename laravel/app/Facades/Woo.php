<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Woo extends Facade
{
    protected static function getFacadeAccessor() {
        return 'WooCommerce';
    }
}