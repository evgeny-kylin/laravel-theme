<?php

namespace App\Helpers;


use App\Models\Page;
use App\Models\Taxonomy;
use Corcel\Model\Option;
use Illuminate\Support\Facades\Route;

class Front
{
    static function route()
    {
        return str_replace('.', ' ', Route::currentRouteName() ?? '');
    }

    static function private_policy_page()
    {
        $id = Option::get('wp_page_for_privacy_policy');
        $page = Page::withoutGlobalScopes()->find($id);
        return $page;
    }

    static function  home_page()
    {
        $id = Option::get('page_on_front');
        $page = Page::withoutGlobalScopes()->find($id);
        return $page;
    }
}