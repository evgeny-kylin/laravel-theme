<?php

namespace App\Helpers;

class Menu
{
    public function getMenu($location)
    {

        if($main_menu = wp_cache_get('menu_'.$location)){
            return $main_menu;
        }
        $nav = get_nav_menu_locations();
        if(!isset($nav[$location])){
            return [];
        }

        $items = wp_get_nav_menu_items($nav[$location]);

        if($items){
            foreach($items as $item){
                if($item->menu_item_parent == 0){
                    $item_menu[$item->ID] = self::get_menu_items($item, $items);
                }
            }
            wp_cache_add( 'menu_'.$location, $item_menu);
            return $item_menu;
        }
        return [];
    }

    static function get_menu_items($item, $items)
    {
        $item_menu = ['title' => $item->title, 'url' => $item->url,'children' => []];
        foreach($items as $subItem){
            if($subItem->menu_item_parent == $item->ID){
                $item_menu['children'][] = ['title' => $subItem->title, 'url' => $subItem->url];
            }
        }
        return $item_menu;
    }
}