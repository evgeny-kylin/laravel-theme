<div>
    <a href="{{ $post->url }}" class="uk-link-reset uk-card uk-card-default uk-card-hover uk-grid-collapse uk-child-width-1-2@s" uk-grid>
        <div class="uk-card-media-left uk-cover-container">
            <img src="{{ $post->thumb['thumbnail']['url'] }}" alt="" uk-cover>
            <canvas width="480" height="480"></canvas>
        </div>
        <div>
            <div class="uk-card-body">
                <h3 class="uk-card-title">{{ $post->post_title }}</h3>
                <div class="uk-text-muted uk-text-small mb-1">
                    <span>Ш: {{ $post->meta->w }}мм</span>,
                    <span>Д: {{ $post->meta->d }}мм</span>,
                    <span>В: {{ $post->meta->h }}мм</span>
                </div>
                <div class="mt-1">
                    @foreach(\Illuminate\Support\Arr::get($post->terms,'assignment', collect()) as $term)
                        <span class="uk-text-meta">#{{ $term->name }}</span>
                    @endforeach
                </div>
            </div>
            <div class="uk-card-footer uk-text-right">
                <b>{{ $post->meta->price }}</b>&nbsp;<i class="far fa-ruble-sign f-16"></i>
            </div>
        </div>
    </a>
</div>