<div class="post-content">
    @if($post->thumb)
        <div class="post-content-image mb-2">
            <img src="{{ $post->thumb['hdm_c']['url'] }}">
        </div>
    @elseif(strlen($post->post_excerpt) > 10)
        <hr>
    @endif
    <div class="post-content-content uk-container-small">
        {!! wpautop($post->post_content) !!}
        @if($post->post_type == 'post')
            <div class="uk-text-muted">
                <i class="far fa-calendar-alt"></i><span class="ml-1">{{ $post->post_date->format('j M Y H:i') }}</span>
            </div>
        @endif
    </div>
</div>