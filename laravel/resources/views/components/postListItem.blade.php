<div>
    <a href="{{ $post->url }}" class="uk-link-reset " uk-grid>
        <div class="uk-width-1-6">
            <div class="uk-cover-container">
                <img src="{{ $post->thumb['thumbnail']['url'] }}" alt="" uk-cover>
                <canvas width="480" height="480"></canvas>
            </div>
        </div>

        <div class="uk-width-5-6">
            <div class="">
                <h3 class="uk-card-title">{{ $post->post_title }}</h3>
            </div>
            <div>
                <span class="uk-text-muted">{{ $post->post_excerpt }}</span>
            </div>
            <div class="uk-text-muted mt-1">
                <i class="far fa-calendar-alt"></i><span class="ml-1">{{ $post->post_date->format('j M Y H:i') }}</span>
            </div>
        </div>
    </a>
</div>