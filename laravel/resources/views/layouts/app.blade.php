<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ asset('storage/logo/logo-16x16.png') }}">
    <link rel="apple-touch-icon" type="image/png" href="{{ asset('storage/logo/logo-57x57.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $controller->getTitle() ?? 'Laravel' }}</title>
    <meta name="description" content="{{ $controller->getDescription() ?? 'Laravel' }}" />

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}@if(config('app.debug'))?t={{ time() }}@endif" />

</head>
<body>
<div id="app">
    @include('components.header')
    @yield('content')
    @include('components.footer')
</div>
<!-- JS -->
<script src="{{ asset('js/app.js') }}@if(config('app.debug'))?t={{ time() }}@endif"></script>
</body>
</html>