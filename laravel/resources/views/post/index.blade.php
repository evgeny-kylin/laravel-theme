@extends('layouts.app')

@section('content')
    <div class="uk-section">
        <div class="uk-container">
            <h1 class="">Блог</h1>
            <span class="uk-text-meta">Назначение</span>
        </div>
    </div>
    <div class="uk-section">
        <div class="uk-container">
            <div class="uk-child-width-1-1" uk-grid>
                    @foreach($posts as $post)
                        @include('components.postListItem')
                    @endforeach
                </div>
                {{ $posts->links('components.pagination') }}
            </div>
        </div>
    </div>
@endsection