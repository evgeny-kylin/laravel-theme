@extends('layouts.app')

@section('content')
    <div class="uk-section">
        <div class="uk-container">
            <h1 class="">{{ $post->post_title }}</h1>
            @include('components.postContent')
        </div>
    </div>
@endsection