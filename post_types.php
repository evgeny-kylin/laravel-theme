<?php

add_action( 'init', 'register_post_type', 1,1 );

function register_post_type() {

}

add_theme_support( 'post-thumbnails' );

add_action('init', 'add_post_excerpt');
function add_post_excerpt(){
    add_post_type_support( 'post', ['excerpt'] );
    add_post_type_support( 'page', ['excerpt'] );
}